<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171102101839 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE config (id INT AUTO_INCREMENT NOT NULL, uniqueness_floor INT NOT NULL, uniquness_ceil INT NOT NULL, water_floor INT NOT NULL, water_ceil INT NOT NULL, spam_floor INT NOT NULL, spam_ceil INT NOT NULL, amount_characters_floor INT NOT NULL, amount_characters_ceil INT NOT NULL, UNIQUE INDEX UNIQ_D48A2F7C9D7056C6 (uniqueness_floor), UNIQUE INDEX UNIQ_D48A2F7CA03E52E6 (uniquness_ceil), UNIQUE INDEX UNIQ_D48A2F7C49B7FFC7 (water_floor), UNIQUE INDEX UNIQ_D48A2F7C268EC733 (water_ceil), UNIQUE INDEX UNIQ_D48A2F7C7DC88329 (spam_floor), UNIQUE INDEX UNIQ_D48A2F7C372BD55A (spam_ceil), UNIQUE INDEX UNIQ_D48A2F7C57AE36F8 (amount_characters_floor), UNIQUE INDEX UNIQ_D48A2F7C3EB590A1 (amount_characters_ceil), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE config');
    }
}
