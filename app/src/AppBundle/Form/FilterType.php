<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 03.11.17
 * Time: 12:26
 */

namespace AppBundle\Form;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterType extends AbstractType
{
    private $em;

    private $container;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $users = $this->em->getRepository('AppBundle:User')->findAll();
        $current_user = $this->container->get('security.token_storage')->getToken()->getUser();
        $user_role = $current_user->getNameAndRole($current_user);

        $select_options = [
            $user_role.' - '.$current_user->getUsername() => $current_user
        ];

        /** create select options depends on user role */
        switch ($user_role) {
            case 'администратор':
                foreach ($users as $user) {
                    if ($role = $user->getNameAndRole($user)) {
                        if ($user->getId() !== $current_user->getId()) {
                            $select_options[$role.' - '.$user->getUsername()] = $user;
                        }
                    }
                }
            break;
            case 'автор': break;
            case 'редактор': break;
            case 'главный редактор':
                foreach ($users as $user) {
                    if ($role = $user->getNameAndRole($user)) {
                        if ($user->getId() !== $current_user->getId() && $role != 'администратор') {
                            $select_options[$role.' - '.$user->getUsername()] = $user;
                        }
                    }
                }
        }

        $date = new \DateTime();
        $days_to_lust_monday = $this->getAmountDaysToLustMonday($date);
        $date_from = clone $date;
        $date_from->sub(\DateInterval::createFromDateString($days_to_lust_monday.' days'));

        $builder
            ->add('user', ChoiceType::class, [
                'choices' => $select_options,
                'label' => false,
                'attr' => ['class' => 'form-control'],
                'data' => 'John Doe',
            ])
            ->add('created_from', null, [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'created_from'
                ],
                'data' => $date_from->format('Y-m-d'),
                'label' => 'с'
            ])
            ->add('created_to', null, [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'created_to'
                ],
                'data' => $date->format('Y-m-d'),
                'label' => 'по'
            ]);
    }

    public function getAmountDaysToLustMonday(\DateTime $date)
    {
        $week_days = [
            'Son' => 0,
            'Mon' => 1,
            'Tue' => 2,
            'Wen' => 3,
            'Thu' => 4,
            'Fri' => 5,
            'Sat' => 6
        ];
        $day = $date->format('D');

        return $week_days[$day] + 6;
    }
}