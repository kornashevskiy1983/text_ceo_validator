<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Validation
 *
 * @ORM\Table(name="validation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ValidationRepository")
 */
class Validation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="uniqueness", type="integer", nullable=true)
     */
    private $uniqueness;

    /**
     * @var int
     *
     * @ORM\Column(name="water", type="integer", nullable=true)
     */
    private $water;

    /**
     * @var int
     *
     * @ORM\Column(name="spam_amount", type="integer", nullable=true)
     */
    private $spamAmount;

    /**
     * @var int
     *
     * @ORM\Column(name="number_characters_with_spaces", type="integer", nullable=true)
     */
    private $numberCharactersWithSpaces;

    /**
     * @var int
     *
     * @ORM\Column(name="number_characters_without_spaces", type="integer", nullable=true)
     */
    private $numberCharactersWithoutSpaces;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="url_access", type="string", length=255, unique=true)
     */
    private $urlAccess;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="validation", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniqueness
     *
     * @param integer $uniqueness
     *
     * @return Validation
     */
    public function setUniqueness($uniqueness)
    {
        $this->uniqueness = $uniqueness;

        return $this;
    }

    /**
     * Get uniqueness
     *
     * @return int
     */
    public function getUniqueness()
    {
        return $this->uniqueness;
    }

    /**
     * Set water
     *
     * @param integer $water
     *
     * @return Validation
     */
    public function setWater($water)
    {
        $this->water = $water;

        return $this;
    }

    /**
     * Get water
     *
     * @return int
     */
    public function getWater()
    {
        return $this->water;
    }

    /**
     * Set spamAmount
     *
     * @param integer $spamAmount
     *
     * @return Validation
     */
    public function setSpamAmount($spamAmount)
    {
        $this->spamAmount = $spamAmount;

        return $this;
    }

    /**
     * Get spamAmount
     *
     * @return int
     */
    public function getSpamAmount()
    {
        return $this->spamAmount;
    }

    /**
     * Set numberCharactersWithSpaces
     *
     * @param integer $numberCharactersWithSpaces
     *
     * @return Validation
     */
    public function setNumberCharactersWithSpaces($numberCharactersWithSpaces)
    {
        $this->numberCharactersWithSpaces = $numberCharactersWithSpaces;

        return $this;
    }

    /**
     * Get numberCharactersWithSpaces
     *
     * @return int
     */
    public function getNumberCharactersWithSpaces()
    {
        return $this->numberCharactersWithSpaces;
    }

    /**
     * Set numberCharactersWithoutSpaces
     *
     * @param integer $numberCharactersWithoutSpaces
     *
     * @return Validation
     */
    public function setNumberCharactersWithoutSpaces($numberCharactersWithoutSpaces)
    {
        $this->numberCharactersWithoutSpaces = $numberCharactersWithoutSpaces;

        return $this;
    }

    /**
     * Get numberCharactersWithoutSpaces
     *
     * @return int
     */
    public function getNumberCharactersWithoutSpaces()
    {
        return $this->numberCharactersWithoutSpaces;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Validation
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set urlAccess
     *
     * @param string $urlAccess
     *
     * @return Validation
     */
    public function setUrlAccess($urlAccess)
    {
        $this->urlAccess = $urlAccess;

        return $this;
    }

    /**
     * Get urlAccess
     *
     * @return string
     */
    public function getUrlAccess()
    {
        return $this->urlAccess;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Validation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

