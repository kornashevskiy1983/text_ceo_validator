<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Config
 *
 * @ORM\Table(name="config")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConfigRepository")
 */
class Config
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="uniqueness_floor", type="integer", unique=true)
     */
    private $uniquenessFloor;

    /**
     * @var int
     *
     * @ORM\Column(name="uniquness_ceil", type="integer", unique=true)
     */
    private $uniqunessCeil;

    /**
     * @var int
     *
     * @ORM\Column(name="water_floor", type="integer", unique=true)
     */
    private $waterFloor;

    /**
     * @var int
     *
     * @ORM\Column(name="water_ceil", type="integer", unique=true)
     */
    private $waterCeil;

    /**
     * @var int
     *
     * @ORM\Column(name="spam_floor", type="integer", unique=true)
     */
    private $spamFloor;

    /**
     * @var int
     *
     * @ORM\Column(name="spam_ceil", type="integer", unique=true)
     */
    private $spamCeil;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_characters_floor", type="integer", unique=true)
     */
    private $amountCharactersFloor;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_characters_ceil", type="integer", unique=true)
     */
    private $amountCharactersCeil;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniquenessFloor
     *
     * @param integer $uniquenessFloor
     *
     * @return Config
     */
    public function setUniquenessFloor($uniquenessFloor)
    {
        $this->uniquenessFloor = $uniquenessFloor;

        return $this;
    }

    /**
     * Get uniquenessFloor
     *
     * @return int
     */
    public function getUniquenessFloor()
    {
        return $this->uniquenessFloor;
    }

    /**
     * Set uniqunessCeil
     *
     * @param integer $uniqunessCeil
     *
     * @return Config
     */
    public function setUniqunessCeil($uniqunessCeil)
    {
        $this->uniqunessCeil = $uniqunessCeil;

        return $this;
    }

    /**
     * Get uniqunessCeil
     *
     * @return int
     */
    public function getUniqunessCeil()
    {
        return $this->uniqunessCeil;
    }

    /**
     * Set waterFloor
     *
     * @param integer $waterFloor
     *
     * @return Config
     */
    public function setWaterFloor($waterFloor)
    {
        $this->waterFloor = $waterFloor;

        return $this;
    }

    /**
     * Get waterFloor
     *
     * @return int
     */
    public function getWaterFloor()
    {
        return $this->waterFloor;
    }

    /**
     * Set waterCeil
     *
     * @param integer $waterCeil
     *
     * @return Config
     */
    public function setWaterCeil($waterCeil)
    {
        $this->waterCeil = $waterCeil;

        return $this;
    }

    /**
     * Get waterCeil
     *
     * @return int
     */
    public function getWaterCeil()
    {
        return $this->waterCeil;
    }

    /**
     * Set spamFloor
     *
     * @param integer $spamFloor
     *
     * @return Config
     */
    public function setSpamFloor($spamFloor)
    {
        $this->spamFloor = $spamFloor;

        return $this;
    }

    /**
     * Get spamFloor
     *
     * @return int
     */
    public function getSpamFloor()
    {
        return $this->spamFloor;
    }

    /**
     * Set spamCeil
     *
     * @param integer $spamCeil
     *
     * @return Config
     */
    public function setSpamCeil($spamCeil)
    {
        $this->spamCeil = $spamCeil;

        return $this;
    }

    /**
     * Get spamCeil
     *
     * @return int
     */
    public function getSpamCeil()
    {
        return $this->spamCeil;
    }

    /**
     * Set amountCharactersFloor
     *
     * @param integer $amountCharactersFloor
     *
     * @return Config
     */
    public function setAmountCharactersFloor($amountCharactersFloor)
    {
        $this->amountCharactersFloor = $amountCharactersFloor;

        return $this;
    }

    /**
     * Get amountCharactersFloor
     *
     * @return int
     */
    public function getAmountCharactersFloor()
    {
        return $this->amountCharactersFloor;
    }

    /**
     * Set amountCharactersCeil
     *
     * @param integer $amountCharactersCeil
     *
     * @return Config
     */
    public function setAmountCharactersCeil($amountCharactersCeil)
    {
        $this->amountCharactersCeil = $amountCharactersCeil;

        return $this;
    }

    /**
     * Get amountCharactersCeil
     *
     * @return int
     */
    public function getAmountCharactersCeil()
    {
        return $this->amountCharactersCeil;
    }
}

