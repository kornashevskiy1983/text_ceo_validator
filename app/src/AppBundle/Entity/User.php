<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.11.17
 * Time: 10:32
 */

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Validation", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $validation;

    /**
     * @return mixed
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @param mixed $validation
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;
    }


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    public function getNameAndRole(User $user)
    {
        $roles = [
            'ROLE_ADMIN' => 'администратор',
            'ROLE_AUTHOR' => 'автор',
            'ROLE_EDITOR' => 'редактор',
            'ROLE_MAINEDITOR' => 'главный редактор'
        ];

        $role = $user->getRoles()[0];

        if (isset($roles[$role])) {
            return $roles[$role];
        }

        return false;
    }
}