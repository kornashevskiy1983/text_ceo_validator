<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.11.17
 * Time: 11:53
 */

namespace AppBundle\Controller;


use AppBundle\Form\FilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $roles = $this->getUser()->getRoles();
        $user = $this->getUser();
        $filter_params = $request->get('0');

        if ($filter_params != null) {
            $filter_params = json_decode(urldecode($filter_params), true);
            $filter_params['user'] = $user;
            $query = $this->getDoctrine()->getRepository('AppBundle:Validation')
                ->createQueryByFilter($filter_params);
        } else {
            $query = $this->getDoctrine()->getRepository('AppBundle:Validation')
                ->getQueryToPagination($roles, $user);
        }


        $config = $em->getRepository('AppBundle:Config')
            ->selectOne();

        $form = $this->createForm(FilterType::class);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $filter_params = $form->getData();
            $query = $this->getDoctrine()->getRepository('AppBundle:Validation')
                ->createQueryByFilter($filter_params);

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1), 10
            );

            return $this->render('@App/admin/index.html.twig', [
                'pagination' => $pagination,
                'config' => $config,
                'form' => $form->createView(),
                'query_params' => [urlencode(json_encode($filter_params))],
                'user_name' => $user->getUsername()
            ]);
        }


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), 10
            );

        return $this->render('@App/admin/index.html.twig', [
            'pagination' => $pagination,
            'config' => $config,
            'form' => $form->createView(),
            'query_params' => [$request->get('0')],
            'user_name' => $user->getUsername()
        ]);
    }
}