<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Validation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need

        return $this->render('@App/default/index.html.twig');
    }

    /**
     * @param Request $request
     * @Route("/seo", name="seo")
     * @return mixed
     */
    public function checkTextAction(Request $request)
    {
        $unique_number = md5(time());
        $redirect_url = $_SERVER['HTTP_REFERER'].'seo/'.$unique_number;
        $result = $this
            ->get('seo_service')
            ->verify($request->get('text'));

        $validation = new Validation();
        $validation->setUser($this->getUser());
        $validation->setCreatedAt(new \DateTime());
        $validation->setText($request->get('text'));
        $validation->setNumberCharactersWithoutSpaces(
            $result['amount_characters_without_space']
        );
        $validation->setNumberCharactersWithSpaces(
            $result['amount_characters_with_space']
        );
        $validation->setUniqueness($result['uniqueness']);
        $validation->setWater($result['water']);
        $validation->setSpamAmount($result['spam_amount']);
        $validation->setUrlAccess(
            $unique_number
        );
        $em = $this->getDoctrine()->getManager();
        $em->persist($validation);
        $em->flush();

        return $this->redirect($redirect_url);
    }

    /**
     * @param $id
     * @Route("/seo/{id}")
     * @return mixed
     */
    public function viewSeoResultAction($id)
    {
        $validation = $this->getDoctrine()->getRepository('AppBundle:Validation')
            ->getByUriAccess($id);

        if ($validation == null) {
            throw $this->createNotFoundException('По данному запросу проверок не найдено');
        }

        $config = $this->getDoctrine()->getRepository('AppBundle:Config')
            ->selectOne();

        $web_classes = $this
            ->get('validation_parser')
            ->getWebStyles($validation, $config);

        return $this->render('@App/default/seo_result.html.twig', [
            'validator' => $validation,
            'config' => $config,
            'web_classes' => $web_classes
        ]);
    }
}
