<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.11.17
 * Time: 17:23
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getUsersByRoles(array $roles)
    {
        $users = $this->getEntityManager()->getRepository('AppBundle:User')
            ->findAll();

        $sorted_users = [];
                            /** @var User $user */
        foreach ($users as $user) {
            foreach ($roles as $role) {
                if ($user->hasRole($role)) {
                    $sorted_users[] = $user;
                }
            }
        }

        return $sorted_users;
    }
}