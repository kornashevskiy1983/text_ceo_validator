<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.11.17
 * Time: 13:40
 */

namespace AppBundle\Service;


class SeoService
{
    public function verify($text)
    {
        $uniqueness = mt_rand(0, 100);
        $water = mt_rand(0, 100);
        $spam_amount = mt_rand(0, 100);
        $amount_characters_with_space = $this->getAmountCharactersWithSpace($text);
        $amount_characters_without_space = $this->getAmountCharactersWithoutSpace($text);

        return [
            'uniqueness' => $uniqueness,
            'water' => $water,
            'spam_amount' => $spam_amount,
            'amount_characters_with_space' => $amount_characters_with_space,
            'amount_characters_without_space' => $amount_characters_without_space
        ];
    }

    private function getAmountCharactersWithSpace($text)
    {
        return strlen(trim($text));
    }

    private function getAmountCharactersWithoutSpace($text)
    {
        return strlen(str_replace(' ', '', $text));
    }
}