<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 02.11.17
 * Time: 14:47
 */

namespace AppBundle\Service;


use AppBundle\Entity\Config;
use AppBundle\Entity\Validation;

class ValidationParser
{
    const VALID = 'green';
    const INVALID = 'red';

    /**
     * @param Validation $validation
     * @param Config $config
     * @return array
     */
    public function getWebStyles(Validation $validation, Config $config)
    {
        $result = [];

        if ($validation->getUniqueness() >= $config->getUniquenessFloor()
            && $validation->getUniqueness() <= $config->getUniqunessCeil()) {
            $result['uniqueness'] = self::VALID;
        } else $result['uniqueness'] = self::INVALID;

        if ($validation->getWater() >= $config->getWaterFloor()
            && $validation->getWater() <= $config->getWaterCeil()) {
            $result['water'] = self::VALID;
        } else $result['water'] = self::INVALID;

        if ($validation->getSpamAmount() >= $config->getSpamFloor()
            && $validation->getSpamAmount() <= $config->getSpamCeil()) {
            $result['spam_amount'] = self::VALID;
        } else $result['spam_amount'] = self::INVALID;

        if ($validation->getNumberCharactersWithoutSpaces() >= $config->getAmountCharactersFloor()
            && $validation->getNumberCharactersWithoutSpaces() <= $config->getAmountCharactersCeil()) {
            $result['number_characters'] = self::VALID;
        } else $result['number_characters'] = self::INVALID;

        $result['container'] = $this->getWebContainerStyle($result);

        return $result;
    }

    /**
     * @param $result
     * @return string
     */
    private function getWebContainerStyle($result)
    {
        foreach ($result as $style) {
            if ($style == self::INVALID) {
                return self::INVALID;
            }
        }

        return self::VALID;
    }
}